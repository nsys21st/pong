//
//  GameScene.swift
//  Pong
//
//  Created by Arief Shaifullah Akbar on 17/05/19.
//  Copyright © 2019 Arief Shaifullah Akbar. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    var backgroundImage = SKSpriteNode()
    var ball = SKSpriteNode()
    var player1 = SKSpriteNode()
    var player2 = SKSpriteNode()
    
    var ballHitSound = SKAction.playSoundFileNamed("ballHit.wav", waitForCompletion: false)
    
    var player1Score = false
    
    var player2ScoreLabel = SKLabelNode()
    var player1ScoreLabel = SKLabelNode()
    
    var border = SKPhysicsBody()
    var bottomBorder = SKSpriteNode()
    var topBorder = SKSpriteNode()
    
    var score = [Int]()
    var firstTouch = false
    
    var winnerPrize = SKSpriteNode()
    var loserPrize = SKSpriteNode()
    
    var textureAtlas = SKTextureAtlas()
    var backgroundTextureArray = [SKTexture]()
    var winnerTextureArray = [SKTexture]()
    var loserTextureArray = [SKTexture]()
    
    var backgroundTextureAtlas = SKTextureAtlas()
    var winnerTextureAtlas = SKTextureAtlas()
    var loserTextureAtlas = SKTextureAtlas()
    
    override func didMove(to view: SKView) {
        
        // set GameScene as delegate
        physicsWorld.contactDelegate = self
        
        startGame()
        
        ball = self.childNode(withName: "ball") as! SKSpriteNode
        
        player2ScoreLabel = self.childNode(withName: "topLabel") as! SKLabelNode
        player1ScoreLabel = self.childNode(withName: "btmLabel") as! SKLabelNode
        
        
        // player1 racket position
        player1 = self.childNode(withName: "player1") as! SKSpriteNode
        player1.position.y = (-self.frame.height / 2) + 100

        // player2 racket position
        player2 = self.childNode(withName: "player2") as! SKSpriteNode
        player2.position.y = (self.frame.height / 2) - 100
        
        bottomBorder = self.childNode(withName: "bottomBorder") as! SKSpriteNode
        bottomBorder.position.y = (-self.frame.height / 2) + 40
        
        topBorder = self.childNode(withName: "topBorder") as! SKSpriteNode
        topBorder.position.y = (self.frame.height / 2) - 40
        
        backgroundTextureAtlas = createTextureAtlas(folder: "background", prefix: "background", array: &backgroundTextureArray)
        winnerTextureAtlas = createTextureAtlas(folder: "images", prefix: "win", array: &winnerTextureArray)
        loserTextureAtlas = createTextureAtlas(folder: "loseImages", prefix: "lose", array: &loserTextureArray)
        
        // create annoying animated background
        backgroundImage = SKSpriteNode(imageNamed: backgroundTextureAtlas.textureNames[0])
        self.addChild(backgroundImage)
        backgroundImage.zPosition = -1.0
        backgroundImage.run(SKAction.repeatForever(SKAction.animate(with: backgroundTextureArray, timePerFrame: 0.07)))
        
        // define border
        border = SKPhysicsBody(edgeLoopFrom: self.frame)
        border.friction = 0
        border.restitution = 1
        border.categoryBitMask = 1
        
        self.physicsBody = border
        
    }
    
    func createTextureAtlas(folder: String, prefix: String, array: inout [ SKTexture ]) -> SKTextureAtlas {
        textureAtlas = SKTextureAtlas(named: folder)
        for i in 1...textureAtlas.textureNames.count {
            let name = prefix + "_\(i).png"
            array.append(SKTexture(imageNamed: name))
        }
        
        return textureAtlas
    }
    
    func player1Win(){
        winnerPrize = SKSpriteNode(imageNamed: winnerTextureAtlas.textureNames[0] )
        winnerPrize.size = CGSize(width: 86, height: 193)
        winnerPrize.position = CGPoint(x: 0, y: -240)
        
        self.addChild(winnerPrize)
    }
    
    func player2Win(){
        winnerPrize = SKSpriteNode(imageNamed: winnerTextureAtlas.textureNames[0] )
        winnerPrize.size = CGSize(width: 86, height: 193)
        winnerPrize.position = CGPoint(x: 0, y: 240)
        winnerPrize.zRotation = 3.1
        
        self.addChild(winnerPrize)
    }
    
    func player1Lose() {
        loserPrize = SKSpriteNode(imageNamed: loserTextureAtlas.textureNames[0])
        loserPrize.size = CGSize(width: 86, height: 193)
        loserPrize.position = CGPoint(x: 0, y: -240)
        
        self.addChild(loserPrize)
    }
    
    func player2Lose() {
        loserPrize = SKSpriteNode(imageNamed: loserTextureAtlas.textureNames[0])
        loserPrize.size = CGSize(width: 86, height: 193)
        loserPrize.position = CGPoint(x: 0, y: 240)
        loserPrize.zRotation = 3.1
        
        self.addChild(loserPrize)
    }
    
    func startGame() {
        score = [0,0]
        player1ScoreLabel.text = "\(score[0])"
        player2ScoreLabel.text = "\(score[1])"
        
        winnerPrize.removeFromParent()
        loserPrize.removeFromParent()
    }
    
    
    func updateScore(playerWhoScored: SKSpriteNode) {
        
        ball.position = CGPoint(x: 0, y: 0)
        ball.physicsBody?.velocity = CGVector(dx: 0, dy: 0)
        
        if playerWhoScored == player1 {
            score[0] += 1
            ball.physicsBody?.applyImpulse(CGVector(dx: 90, dy: 90))
            
            player1Score = true
            
        } else if playerWhoScored == player2 {
            score[1] += 1
            ball.physicsBody?.applyImpulse(CGVector(dx: -90, dy: -90))
            
            player1Score = false
            
        }
        
        player1ScoreLabel.text = "\(score[0])"
        player2ScoreLabel.text = "\(score[1])"
        result(score: score)
    }
    
    func result(score: [Int]) {
        if score[0] == 15 {
            
            ball.position = CGPoint(x: 0, y: 0)
            ball.physicsBody?.velocity = CGVector(dx: 0, dy: 0)
            
            firstTouch = false
            
            player1Win()
            winnerPrize.run(SKAction.repeatForever(SKAction.animate(with: winnerTextureArray, timePerFrame: 0.07)))
            
            player2Lose()
            loserPrize.run(SKAction.repeatForever(SKAction.animate(with: loserTextureArray, timePerFrame: 0.07)))
            
        } else if score[1] == 15 {
            
            ball.position = CGPoint(x: 0, y: 0)
            ball.physicsBody?.velocity = CGVector(dx: 0, dy: 0)
            
            firstTouch = false
            
            player2Win()
            winnerPrize.run(SKAction.repeatForever(SKAction.animate(with: winnerTextureArray, timePerFrame: 0.07)))
            
            player1Lose()
            loserPrize.run(SKAction.repeatForever(SKAction.animate(with: loserTextureArray, timePerFrame: 0.07)))
        }
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if firstTouch == false {
            startGame()
            
            ball.physicsBody?.applyImpulse(CGVector(dx: 90, dy: 90))
            
            firstTouch = true
        }

        
        for touch in touches {
            let location = touch.location(in: self)
            
            if location.y < 0 {
                player1.run(SKAction.moveTo(x: location.x, duration: TimeInterval(CGFloat(Float(arc4random()) / Float(UINT32_MAX)))))

            } else {
//                player2.run(SKAction.moveTo(x: location.x, duration: TimeInterval(CGFloat(Float(arc4random()) / Float(UINT32_MAX)))))

            }
        }
        
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            let location = touch.location(in: self)
            if location.y < 0 {
                player1.run(SKAction.moveTo(x: location.x, duration: TimeInterval(CGFloat(Float(arc4random()) / Float(UINT32_MAX)))))
                
            } else {
//                player2.run(SKAction.moveTo(x: location.x, duration: TimeInterval(CGFloat(Float(arc4random()) / Float(UINT32_MAX)))))
                
            }
        }
    }
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
        
        // ai will switch to expert if player score is 14
        if score[0] > 13 {
            if ball.position.y > 0 {
                player2.run(SKAction.moveTo(x: ball.position.x, duration: 0.02))
            } else {
                player2.run(SKAction.moveTo(x: 0, duration: 0.5))
            }
        } else {
            if ball.position.y > 0 {
                player2.run(SKAction.moveTo(x: ball.position.x, duration: 1))
            } else {
                player2.run(SKAction.moveTo(x: 0, duration: 0.5))
            }
        }
        
        if ball.position.y <= player1.position.y - 40 {
            
            updateScore(playerWhoScored: player2)
            
        } else if ball.position.y >= player2.position.y + 40 {
            
            updateScore(playerWhoScored: player1)
            
        }
    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        
        if contact.bodyA.categoryBitMask == 1 && contact.bodyB.categoryBitMask == 2 {
            playSound(sound: ballHitSound)
        }
    }
    
    
    func playSound(sound : SKAction)
    {
        run(sound)
    }
}
